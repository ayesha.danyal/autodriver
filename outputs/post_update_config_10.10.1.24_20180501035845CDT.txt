Building configuration...

Current configuration : 1569 bytes
!
! Last configuration change at 08:55:01 UTC Tue May 1 2018 by cisco
!
version 16.6
service timestamps debug datetime msec
service timestamps log datetime msec
platform qfp utilization monitor load 80
no platform punt-keepalive disable-kernel-core
platform console virtual
!
hostname csr1kv-24
!
boot-start-marker
boot-end-marker
!
!
enable password ********
!
no aaa new-model
!
!
!
!
!
!
!
ip domain name irsols.com
!
!
!
!
!
!
!
!
!
!
subscriber templating
! 
! 
! 
! 
!
!
!
multilink bundle-name authenticated
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
license udi pid CSR1000V sn 97QOX65FLH1
diagnostic bootup level minimal
file prompt quiet
spanning-tree extend system-id
!
!
!
username cisco password 0 ********
!
redundancy
!
!
!
!
!
!
! 
!
!
!
!
!
!
!
!
!
!
!
!
! 
! 
!
!
interface GigabitEthernet1
 ip address 10.10.1.24 255.255.255.0
 negotiation auto
 no mop enabled
 no mop sysid
!
interface GigabitEthernet2
 no ip address
 shutdown
 negotiation auto
 no mop enabled
 no mop sysid
!
interface GigabitEthernet3
 no ip address
 shutdown
 negotiation auto
 no mop enabled
 no mop sysid
!
!
virtual-service csr_mgmt
!
ip forward-protocol nd
no ip http server
no ip http secure-server
ip http client source-interface GigabitEthernet1
ip tftp source-interface GigabitEthernet1
!
!
!
!
!
!
control-plane
!
!
!
!
!
!
line con 0
 stopbits 1
line vty 0
 login local
 transport input ssh
line vty 1
 login local
 length 0
 transport input ssh
line vty 2 4
 login local
 transport input ssh
!
wsma agent exec
!
wsma agent config
!
wsma agent filesys
!
wsma agent notify
!
!
end