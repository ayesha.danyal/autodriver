Building configuration...

Current configuration : 4556 bytes
!
! Last configuration change at 17:39:19 UTC Tue May 1 2018 by cisco
! NVRAM config last updated at 08:59:27 UTC Tue May 1 2018 by cisco
!
version 16.6
service timestamps debug datetime msec
service timestamps log datetime msec
platform qfp utilization monitor load 80
no platform punt-keepalive disable-kernel-core
platform console virtual
!
hostname csr1kv-node5
!
boot-start-marker
boot system flash bootflash:csr1000v-universalk9.16.08.01a.SPA.bin
boot-end-marker
!
!
enable secret 5 $1$MyMz$.OK6m0wdbWiBTabDG7/c4.
!
no aaa new-model
!
!
!
!
!
!
!
ip domain name irsols.com
!
!
!
!
!
!
!
!
!
!
subscriber templating
! 
! 
! 
! 
!
!
!
multilink bundle-name authenticated
!
!
!
!
!
crypto pki trustpoint TP-self-signed-4288310170
 enrollment selfsigned
 subject-name cn=IOS-Self-Signed-Certificate-4288310170
 revocation-check none
 rsakeypair TP-self-signed-4288310170
!
!
crypto pki certificate chain TP-self-signed-4288310170
 certificate self-signed 01
  30820330 30820218 A0030201 02020101 300D0609 2A864886 F70D0101 05050030 
  31312F30 2D060355 04031326 494F532D 53656C66 2D536967 6E65642D 43657274 
  69666963 6174652D 34323838 33313031 3730301E 170D3138 30313035 31383539 
  33395A17 0D323030 31303130 30303030 305A3031 312F302D 06035504 03132649 
  4F532D53 656C662D 5369676E 65642D43 65727469 66696361 74652D34 32383833 
  31303137 30308201 22300D06 092A8648 86F70D01 01010500 0382010F 00308201 
  0A028201 0100B96F 699CE0C7 0DA65BAA FA1696E9 99C618FB D64B68D0 58AEF37C 
  D057E459 7A96EDCB 35BA20A1 2E3D70BE BD9F2AC9 5C4B88C7 28B89C96 57B02ECA 
  3C063125 D0880AB4 5AA64191 95D16FEF 033E353E 52311647 DF7A2F9A 01788F12 
  94B4B422 89353CED 3478E5B3 1EB6A977 4C6B4C0D 2AB13775 745EBE92 2CD7B01E 
  18AD9263 2A16E7F6 A205A552 8A1BD26C BB598F4A C1C6BFCC E263D31F 8CA0CEBA 
  EC9BDD93 B2528FB5 4CEEBB59 E0EDD633 80D13BE5 E56FEEDD 164F1B73 8D6C47F1 
  98E2F64F 92A1B835 C3447511 1B4E3152 AB8E1D5D 02B6F29D E9F189B7 EB2654E9 
  F09907E6 AE3C4BAA 022574ED BAEF4776 3B5B69C5 6B1B0AA8 CFF3C118 3918286B 
  DC4C964E 1B1B0203 010001A3 53305130 0F060355 1D130101 FF040530 030101FF 
  301F0603 551D2304 18301680 1484D7F5 99BD8ED8 404D5B74 0CDCE98B 62DB7C95 
  95301D06 03551D0E 04160414 84D7F599 BD8ED840 4D5B740C DCE98B62 DB7C9595 
  300D0609 2A864886 F70D0101 05050003 82010100 165905B9 6359D834 FAE8BE7D 
  1025B528 58876B14 5B65D3CC 91BC0A18 00F2FE8F 57EC0CD0 9298CDAF 5FDC9A09 
  B5CD5813 1226B3F7 2677F678 EA06D252 1A335270 FE227BD8 22C142D7 DC535513 
  F4F0AB72 BA0A2E80 47D23415 B3C8588B AE08868A 83CE402A 964316BC E953174A 
  D7296B0F 895ECA29 72719991 D06740CD 9C9D0853 C219128D C819C8AD 355906F5 
  4E385129 5D59ADB2 1FF16E0C 34B4C12E EEA23B56 5A439B09 E16D2CC5 9A1DAD4D 
  CC2D93ED A90ED694 89CB2A37 C36ED19A 7D921EA8 2A138B72 CFA44414 6A827E18 
  AC3C2943 85197C10 65DE0B79 3DD58A1E 94FEFC03 9EF6E195 989E0CE9 F5B79C01 
  C0345742 A1AED426 A26CEE96 0283AF3A 88C19A77
  	quit
!
!
!
!
!
!
!
!
!
license udi pid CSR1000V sn 964C7HK7MJQ
license accept end user agreement
diagnostic bootup level minimal
file prompt quiet
spanning-tree extend system-id
!
!
!
username cisco password 0 ********
!
redundancy
!
!
!
!
!
lldp run
!
cdp run
! 
!
!
!
!
!
!
!
!
!
!
!
!
! 
! 
!
!
interface VirtualPortGroup0
 ip address 192.168.35.1 255.255.255.0
 ip nat inside
 no mop enabled
 no mop sysid
!
interface GigabitEthernet1
 ip address 10.10.1.25 255.255.255.0
 ip nat outside
 negotiation auto
 no mop enabled
 no mop sysid
!
interface GigabitEthernet2
 no ip address
 shutdown
 negotiation auto
 no mop enabled
 no mop sysid
!
interface GigabitEthernet3
 no ip address
 shutdown
 negotiation auto
 no mop enabled
 no mop sysid
!
!
virtual-service csr_mgmt
!
iox
ip nat pool Net192_35 192.168.35.1 192.168.35.10 netmask 255.255.255.0
ip nat inside source static tcp 192.168.35.2 7023 10.10.1.203 7023 extendable
ip nat inside source list NAT_ACL interface GigabitEthernet1 overload
ip nat outside source list NAT_ACL pool Net192_35 add-route
ip forward-protocol nd
no ip http server
ip http authentication local
ip http secure-server
ip http client source-interface GigabitEthernet1
ip route 0.0.0.0 0.0.0.0 10.10.1.1
ip route 10.10.1.0 255.255.255.0 10.10.1.1
!
ip ssh version 2
!
!
ip access-list standard NAT_ACL
 permit 192.168.0.0 0.0.255.255
!
!
!
!
control-plane
!
!
!
!
!
!
line con 0
 stopbits 1
line vty 0 4
 exec-timeout 0 0
 password cisco
 login local
 transport input ssh
!
ntp server pool.ntp.org
ntp server time.nist.gov
ntp server time-pnp.cisco.comi.
wsma agent exec
!
wsma agent config
!
wsma agent filesys
!
wsma agent notify
!
!
end