#!/bin/bash

export hiveopsversion="0.8"

if [ "$#" -ne 5 ]
then 
echo -e " Welcome to FogHive Software Defined Ops  "
echo "Usage: "
echo "upgrade-os --[1 $ orch_stage] [3 $ device_family] [4 $ device_list] [5 $ dev_creds] [6 $ filename]"
echo "upgrade-os --push/commit cisco-csrs rtr-list.txt rtr-creds.txt ios_16.8.1.bin"
echo "upgrade-os --commit cisco-csrs rtr-list.txt rtr-creds.txt ios_16.bin"
echo "upgrade-os --push  cisco-csrs rtr-list.txt rtr-creds.txt ios_16.bin"
echo
exit
fi


# upgrade-os --[1 $orch_stage] [2 $orch_operation] [3 $device_family] [4 $device_list] [5 $dev_creds] [6 $filename]
# upgrade-os --push/commit upgrade-os/update-config cisco-csrs rtr-list.txt rtr-creds.txt config-file
export master_config_file="master.cfg"
export orch_stage=$1
export device_type=$2 #
export device_list=$3
export dev_creds=$4
export op_filename=$5

check-target-list() {
# Check if the hosts/target files exist
#export device_list=$1
  if [  -e $device_list ];then
    export num_devices=`grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}' $device_list | wc -l`
    echo -e " Target device file = $device_list "
    echo -e " Total of  $num_devices in  $device_type devices .."
  else
    echo " Target devices list file doesnt exist or wrong name specified, see README for instructions "
    echo " ERROR: please provide correct file with path"
    exit
  fi
}

check-cred-file() {
# Check if the hosts/target files exist
#export device_list=$1
  if [  -e $device_creds ];then
    #export num_devices=`grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}' $device_list | wc -l`
    echo -e " Using credentials from  = $dev_creds "
  else
    echo " Device credentials file doesnt exist or wrong name specified, see README for instructions "
    echo " ERROR: please provide correct file with path"
    exit
  fi
}

check-op-filename() {
# Check if the hosts/target files exist
#export device_list=$1
  if [  -e $op_filename ];then
    #export num_devices=`grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}' $device_list | wc -l`
    echo -e " Using operational file = $op_filename "
  else
    echo "Operations file doesnt exist or wrong name specified, see README for instructions "
    echo "ERROR: please provide correct file with path"
    exit
  fi
}
#ansible-playbook -vvvv /etc/ansible/playbooks/asa_sh_run.yml > /tmp/lastplay.log &
#progress-bar 100&
# Check ticketing system configuration including the API username/pwd exist or not 
# quit and force user to specify in the master.cfg file
check-ticket-system() {
 if [  -e $master_config_file ];then
    #export num_devices=`grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}' $device_list | wc -l`
    echo -e " Using system configurations from $master_config_file"
  else
    echo "Master Config file doesnt exist or has wrong configurations, see README for instructions "
    echo "ERROR: please provide correct file with path"
    exit
  fi

}


if [[ $orch_stage == "--push" ]];then
  export orch_stage="push"
  echo "push selected "
  echo
  check-target-list $device_list
  check-cred-file $dev_creds
  check-op-filename $op_filename
  echo "starting push process"
  #exit
elif [[ $1 == "--commit" ]];then
  echo " commit selected"
 # export orch_stage="commit"
  check-target-list $device_list
  check-cred-file $dev_creds
  check-op-filename $op_filename
  echo " starting commit process"
  check-ticket-system
  exit
else 
  echo " incorrect orch_stage specified .. exiting"
  exit
fi







